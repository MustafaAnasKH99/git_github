# **Mustafa Anas**
============

E-mail: ad16mkha@uwcad.it

Website: [http://mustafaanas.codi03.ml/](http://mustafaanas.codi03.ml/)

Phone: +96170064936

Address: Aley  
Mount of Lebanon

![my photo](myPhoto.jpg)


# **Work experience**
---------------

### Codi 10 / 09 / 2018 — Present

Programming Trainee

# **Qualifications**
--------------

I studied in Lebanon for almost three years. 

Then studied the last section of my high-school (IB Diploma) in Italy.

Currently I'm back in Lebanon learning how to code.  

# **Education**
---------

### International Baccalaureatte Aug 2016 — May 2018

### UWC Adriatic

# **Interests**
---------

I have always been interested in Photography, filmmaking, and technology in general!

I started reading about how computers work and function since I was 13 Y.O

Now my intention is to major in Computer Science (while keeping my work with the camera)

  

# **References**
---------

References available upon request.

Made with [CV Maker](https://cvmkr.com/)

